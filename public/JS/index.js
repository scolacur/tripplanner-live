var $addButtons = $('#itinerary-choices').find('.btn-circle');
var $addDayBtns = $('.add-day');
var days = 2;

$addButtons.on('click', function(){
  //console.log('clicked');
  var currentDay = 1 + $('.day-btn').index($('.current-day'));
  var id = $('.current-day').attr('id');
  var $option = $(this).parent().find('option:selected');

  var list = $('ul').filter('.'+$option.attr('class')).find('span');


  var exists = false;
  for(var i = 0; i<list.length; i++){
    if($(list[i]).text() == $option.text() && $(list[i]).parent().hasClass(id)){
      exists=true;
    }
  }

  if(!exists){
    var $entry1 = $('<div class="itinerary-item"></div>');
    var $entry2 = $('<span class="title"></span>');

    $entry1.addClass(id);
    $entry2.text($option.text());

    var $entry3 = $('<button class="btn btn-xs btn-danger remove btn-circle">x</button>');
    var $entry = $entry1.append($entry2).append($entry3);
    var target = $('ul').filter('.'+$option.attr('class')).append($entry);

  //  console.log($('ul').filter('.'+$option.attr('class')));
    //find latitude and longitude of $option
    var index = $option.index();
    var arrays = {
      hotels: all_hotels,
      activities: all_activities,
      restaurants: all_restaurants
    };
    //figure out what kind of option it is
    var targetId = $option.attr('class'); //"hotels" "activities" or "restaurants"
    var latLong = [];
    var arrayItem = arrays[targetId][index];
    latLong[0] = arrays[targetId][index].place[0].location[0];
    latLong[1] = arrays[targetId][index].place[0].location[1];

    // get the maps div's HTML obj
    var map_canvas_obj = document.getElementById("map-canvas");

    //pass along the name and day
    latLong.push(id);
    latLong.push($option.text());

    var moreInfo;
    if (targetId === "hotels") {
      moreInfo = '<p><b>Stars: </b>' + arrayItem.num_stars + '</p>'+
      '<p><b>Amenities: </b>' + arrayItem.amenities + '</p>';
    }
    else if (targetId==="activities"){
      moreInfo = '<p><b>Age Range: </b>' + arrayItem.age_range + '</p>';
    }
    else if (targetId==="restaurants"){
      moreInfo = '<p><b>Price: </b>' + arrayItem.price + '</p>';
      console.log(arrays[targetId]);
    }

    var contentString =  '<div id="content">'+
      '<div id="siteNotice">'+
      '</div>'+
      '<h3 id="firstHeading" class="firstHeading">'+$option.text()+'</h3>'+
      '<div id="bodyContent">'+ moreInfo +
      '</div>'+
      '</div>';

    //  console.log(contentString);

      latLong.push(contentString);
    if (targetId === "hotels") {
      hotelLocation = latLong;
    }
    else if (targetId==="activities"){
      activityLocations.push(latLong);
    }
    else if (targetId==="restaurants"){
      restaurantLocations.push(latLong);
    }

    initialize_gmaps();
  }
});

//removing items
$('.list-group').on('click', '.remove', function(){
  var targetList = $(this).parent().parent();
  var index;
  var today = $('.current-day').attr('id');

  function checkName(location, i){
    if(location[3] == $(this).prev().text() && location[2] == today){
      index = i;
    }
  }
  if(targetList.hasClass('restaurants')){
    restaurantLocations.forEach(checkName, this);
    restaurantLocations.splice(index, 1);
  }
  else if(targetList.hasClass('hotels')){
    hotelLocation.forEach(checkName, this);
    hotelLocation.splice(index, 1);
  }
  else if(targetList.hasClass('activities')){
    activityLocations.forEach(checkName, this);
    activityLocations.splice(index, 1);
  }

  $(this).parent().remove();
  initialize_gmaps();
});

//adding days
$addDayBtns.on('click', function(){
  var currentDay =  $('.day-btn').length;

  var $newBtn = $('<button class="btn btn-circle day-btn"></button>');
  $newBtn.text(currentDay);
  $('.current-day').removeClass('current-day');
  $newBtn.addClass('current-day');
  $newBtn.attr('id', days);

  $('.itinerary-item').hide();
  $('.itinerary-item').filter('.'+days).show();
  days++;
  $newBtn.insertBefore(this);
  currentDay = 1 + $('.day-btn').index($('.current-day'));

  $('#day-title').find('span').text('Day ' + currentDay);
});

//switching between days
$('.day-buttons').on('click', '.day-btn', function(){
  if(!$(this).hasClass('add-day')){
    $('.current-day').removeClass('current-day');
    $(this).addClass('current-day');
    var currentDay = 1 + $('.day-btn').index($('.current-day'));
    var id = $('.current-day').attr('id');
    $('.itinerary-item').hide();
    $('.itinerary-item').filter('.'+id).show();
    $('#day-title').find('span').text('Day ' + currentDay);
    initialize_gmaps();
  }
});

//deleting a day
$("#day-title").on('click', '.remove', function(){
    var id = $('.current-day').attr('id');
    var index;

    function checkName(location, i){
      if(location[2] == id){
        index = i;
      }
    }
    restaurantLocations.forEach(checkName, this);
    restaurantLocations.splice(index, 1);

    hotelLocation.forEach(checkName, this);
    hotelLocation.splice(index, 1);

    activityLocations.forEach(checkName, this);
    activityLocations.splice(index, 1);

    var currentDay = 1 + $('.day-btn').index($('.current-day'));
    $('.'+currentDay).remove();
    var previousDay = currentDay-1;
    $('#day-title').find('span').text('Day ' + previousDay);
    previousDay--;
    $('.current-day').remove();
    $('.day-btn:eq('+previousDay+')').addClass('current-day');
    $('.itinerary-item:eq('+id+')').remove();
    $('.itinerary-item').hide();
    $('.itinerary-item').filter('.'+currentDay).show();

    $('.day-btn').each(function(i, value){
    //  console.log(i, value, this);
      if(!$(this).hasClass('add-day')){
        $(this).text(i+1);
      }
    });

    //console.log(restaurantLocations);
    initialize_gmaps();

});

///initially was in that layout file
var hotelLocation = [];
var restaurantLocations = [];
var activityLocations = [];
  function initialize_gmaps() {
  // initialize new google maps LatLng object
  var myLatlng = new google.maps.LatLng(40.705189,-74.009209);
  // set the map options hash
  var mapOptions = {
    center: myLatlng,
    zoom: 13,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    styles: styleArr
  };
  // get the maps div's HTML obj
  var map_canvas_obj = document.getElementById("map-canvas");
  // initialize a new Google Map with the options
  var map = new google.maps.Map(map_canvas_obj, mapOptions);
  // Add the marker to the map
  var marker = new google.maps.Marker({
    position: myLatlng,
    title:"Hello World!"
  });

  // draw some locations on the map

  function drawLocation (location, opts) {
    if (typeof opts !== 'object') {
      opts = {};
    }

    var id = $('.current-day').attr('id');

    if(id === location[2]){
      opts.position = new google.maps.LatLng(location[0], location[1]);
      opts.map = map;
      var marker = new google.maps.Marker(opts);
      var infowindow = new google.maps.InfoWindow({
        content: location[4]
      });

      marker.addListener('click', function(){
        infowindow.open(map, marker);
      });
    }
    //compare day of latlng to current day

  }

  console.log(hotelLocation);
  drawLocation(hotelLocation, {
    icon: '/images/lodging_0star.png'
  });
  restaurantLocations.forEach(function (loc) {
    drawLocation(loc, {
      icon: '/images/restaurant.png'
    });
  });
  activityLocations.forEach(function (loc) {
    drawLocation(loc, {
      icon: '/images/star-3.png'
    });
  });
}

$(document).ready(function() {
  initialize_gmaps();
});

var styleArr = [{
  featureType: "landscape",
  stylers: [{
    saturation: -100
  }, {
    lightness: 60
  }]
}, {
  featureType: "road.local",
  stylers: [{
    saturation: -100
  }, {
    lightness: 40
  }, {
    visibility: "on"
  }]
}, {
  featureType: "transit",
  stylers: [{
    saturation: -100
  }, {
    visibility: "simplified"
  }]
}, {
  featureType: "administrative.province",
  stylers: [{
    visibility: "off"
  }]
}, {
  featureType: "water",
  stylers: [{
    visibility: "on"
  }, {
    lightness: 30
  }]
}, {
  featureType: "road.highway",
  elementType: "geometry.fill",
  stylers: [{
    color: "#ef8c25"
  }, {
    lightness: 40
  }]
}, {
  featureType: "road.highway",
  elementType: "geometry.stroke",
  stylers: [{
    visibility: "off"
  }]
}, {
  featureType: "poi.park",
  elementType: "geometry.fill",
  stylers: [{
    color: "#b6c54c"
  }, {
    lightness: 40
  }, {
    saturation: -40
  }]
}];
